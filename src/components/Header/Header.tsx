//import liraries
import React from "react";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Button, Input, Avatar, Dropdown, Menu } from "antd";
import {
  UserOutlined,
  MenuOutlined,
  PlusCircleOutlined,
} from "@ant-design/icons";

import { setPage, setSearch, setBlog } from "../../redux/actions/Payload";
import { RootState } from "../../redux/reducers/CombineReducers";
import { appState } from "../../redux/reducers/appReducer";
import "./Header.scss";

// create a component named Header
const Header = () => {
  const app: appState = useSelector((state: RootState) => state.app);

  const actions = bindActionCreators(
    { setPage, setSearch, setBlog },
    useDispatch()
  );

  const { user, blogs } = app;
  const { Search } = Input;

  const history = useHistory();

  /** -------------------   METHODS   ------------------- */

  const handleSearch = (e: any) => {
    actions.setSearch(e.target.value);
  };

  const handleYourBlogs = () => {
    actions.setPage("/your-blogs");
    history.push("/your-blogs");
  };

  const handleBlogs = () => {
    actions.setPage("/");
    history.push("/");
  };

  const handleLogout = () => {
    history.push("/signin");
  };

  const handleWriteBlog = () => {
    actions.setBlog({
      id: blogs.length + 1,
      userId: user.id,
      title: "",
      description: "",
    });

    history.push("/write-blog");
  };

  const menu = (
    <Menu>
      <Menu.Item>
        <Button type="text">{user && user.name}</Button>
      </Menu.Item>
      <Menu.Item>
        <Button type="link" danger onClick={handleLogout}>
          Log out
        </Button>
      </Menu.Item>
    </Menu>
  );

  const sandwhichMenu = (
    <div className="sandwhich-dropdown">
      <Button type="link" size="large" onClick={handleBlogs}>
        Blogs
      </Button>
      <Button type="link" size="large" onClick={handleYourBlogs}>
        Your Blogs
      </Button>
      <Button type="text" size="large">
        {user && user.name}
      </Button>
      <Button type="link" size="large" danger onClick={handleLogout}>
        Log out
      </Button>
    </div>
  );

  return (
    <header className="header">
      <Button.Group
        className="desktop-menu"
        style={{ minWidth: "25vw", justifyContent: "space-evenly" }}
      >
        <Button type="link" size="large" onClick={handleBlogs}>
          Blogs
        </Button>
        <Button type="link" size="large" onClick={handleYourBlogs}>
          Your Blogs
        </Button>
      </Button.Group>
      <div>
        <Dropdown
          className="sandwhich-menu"
          overlay={sandwhichMenu}
          placement="bottomCenter"
          trigger={["click"]}
        >
          <Button type="link" size="large">
            <MenuOutlined />
          </Button>
        </Dropdown>
      </div>
      <div>
        <Button
          className="sandwhich-menu"
          type="link"
          onClick={handleWriteBlog}
        >
          <PlusCircleOutlined style={{ fontSize: 24 }} />
        </Button>
      </div>
      <div className="search desktop-menu">
        <Search
          placeholder="Search blogs"
          enterButton="Search"
          size="middle"
          style={{ maxWidth: "25vw" }}
          onPressEnter={handleSearch}
          onSearch={handleSearch}
          onChange={handleSearch}
        />
        <Dropdown
          className="desktop-menu"
          overlay={menu}
          placement="bottomCenter"
        >
          <Avatar
            icon={<UserOutlined />}
            style={{ backgroundColor: "#1690ff" }}
          />
        </Dropdown>
      </div>
    </header>
  );
};

//make this component available to the app
export default Header;
