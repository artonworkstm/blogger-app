//import liraries
import React from "react";
import { bindActionCreators } from "redux";
import { Form, Input, Button, message } from "antd";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import { setToken, setRegister, signupType } from "../../redux/actions/Payload";
import { RootState } from "../../redux/reducers/CombineReducers";
import { registerUser } from "../../API_Calls";
import "./Signup.scss";

// create a component named Signup
const Signup = () => {
  const register: signupType = useSelector(
    (state: RootState) => state.register
  );

  const { email, firstName, lastName, password, repassword } = register;

  const history = useHistory();

  const actions = bindActionCreators(
    {
      setToken,
      setRegister,
    },
    useDispatch()
  );

  /** --------------------------    METHODS   -------------------------- */

  const onFinish = async () => {
    if (password === repassword) {
      const name = firstName + " " + lastName;
      const token = await registerUser(email, password, name);
      if (token) {
        actions.setToken(token);
        sessionStorage.setItem("firstLogged", "true");
        history.push("/");
      }
    }
  };

  const onFinishFailed = () => {
    message.error("Authentication error!");
  };

  const handleEmail = (e: any) =>
    actions.setRegister({
      email: e.target.value,
      firstName,
      lastName,
      password,
      repassword,
    });

  const handleFirstName = (e: any) =>
    actions.setRegister({
      email,
      firstName: e.target.value,
      lastName,
      password,
      repassword,
    });

  const handleLastName = (e: any) =>
    actions.setRegister({
      email,
      firstName,
      lastName: e.target.value,
      password,
      repassword,
    });

  const handlePassword = (e: any) =>
    actions.setRegister({
      email,
      firstName,
      lastName,
      password: e.target.value,
      repassword,
    });

  const handleRePassword = (e: any) =>
    actions.setRegister({
      email,
      firstName,
      lastName,
      password,
      repassword: e.target.value,
    });

  const handleRedirect = () => {
    history.push("/signin");
  };

  return (
    <div className="sign">
      <h1 className="title">Blogg-up</h1>
      <Form
        className="form"
        name="signup-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item>
          <Input placeholder="E-mail" onChange={handleEmail} />
        </Form.Item>
        <Form.Item>
          <Input.Group>
            <Input placeholder="First Name" onChange={handleFirstName} />
            <Input placeholder="Last Name" onChange={handleLastName} />
          </Input.Group>
        </Form.Item>
        <Form.Item>
          <Input.Group>
            <Input.Password placeholder="Password" onChange={handlePassword} />
            <Input.Password
              placeholder="Password again"
              onChange={handleRePassword}
            />
          </Input.Group>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" style={{ width: "100%" }}>
            Signup
          </Button>
        </Form.Item>
        <Form.Item>
          <Button
            type="link"
            htmlType="button"
            style={{
              width: "100%",
              padding: 0,
              display: "flex",
              justifyContent: "flex-start",
            }}
            onClick={handleRedirect}
          >
            Already have an account ? Login here
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

//make this component available to the app
export default Signup;
