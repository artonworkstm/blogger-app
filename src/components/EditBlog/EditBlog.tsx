//import liraries
import React from "react";
import { bindActionCreators } from "redux";
import { SaveOutlined } from "@ant-design/icons";
import { Card, Select, Button, Tooltip } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";

import { RootState } from "../../redux/reducers/CombineReducers";
import { updateBlog, writeBlog, Blog } from "../../API_Calls";
import { appState } from "../../redux/reducers/appReducer";
import { setBlog } from "../../redux/actions/Payload";
import TextArea from "antd/lib/input/TextArea";
import Header from "../Header/Header";
import "./EditBlog.scss";

// create a component named EditBlog
const EditBlog = () => {
  const editBlog = useSelector((state: RootState) => state.editBlog);
  const app: appState = useSelector((state: RootState) => state.app);

  const { token, user } = app;
  const { id, userId, title, description } = editBlog;
  const { Option } = Select;

  const history = useHistory();
  const location = useLocation();

  const actions = bindActionCreators({ setBlog }, useDispatch());

  const handleSave = () => {
    const blog: Blog = {
      id,
      userId,
      title,
      description,
    };

    if (location.pathname === "/write-blog") {
      writeBlog(token, blog);
    } else {
      updateBlog(token, blog);
    }
    history.push("/blogs");
  };

  const handleTitleChange = (e: any) =>
    actions.setBlog({ id, userId, title: e.target.value, description });

  const handleDescChange = (e: any) =>
    actions.setBlog({ id, userId, title, description: e.target.value });

  const handleUserIdChange = (e: number) =>
    actions.setBlog({
      id: id,
      userId: e,
      title,
      description,
    });

  const saveTip = <span>Save</span>;

  const titleInput = (
    <TextArea
      defaultValue={title}
      placeholder="Title"
      autoSize={{ minRows: 1, maxRows: 1 }}
      onChange={handleTitleChange}
    />
  );

  return (
    <div className="blog-container">
      <Header />
      <Card title={titleInput} className="blog-item">
        <TextArea
          defaultValue={description}
          placeholder="Description"
          autoSize
          onChange={handleDescChange}
          onBlur={(e) => console.log(e)}
        />
        <div className="footer-edit">
          <Select
            placeholder="Author displayed"
            defaultValue={user && user.id}
            style={{ width: "100%" }}
            onSelect={handleUserIdChange}
          >
            <Option value={user && user.id}>{user && user.name}</Option>
            <Option value={0}>Anonymus</Option>
          </Select>

          <Tooltip placement="top" title={saveTip}>
            <Button
              className="save-button"
              type="primary"
              style={{ width: "20%", minWidth: "4rem" }}
              onClick={handleSave}
            >
              <SaveOutlined style={{ fontSize: 22 }} />
            </Button>
          </Tooltip>
        </div>
      </Card>
    </div>
  );
};

//make this component available to the app
export default EditBlog;
