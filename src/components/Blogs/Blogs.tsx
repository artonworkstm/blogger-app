//import liraries
import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { Affix, Button, message } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { PlusCircleOutlined } from "@ant-design/icons";
import { useHistory, useLocation } from "react-router-dom";

import { getUserInfo, getBlogs, Blog, getComments } from "../../API_Calls";
import { RootState } from "../../redux/reducers/CombineReducers";
import { appState } from "../../redux/reducers/appReducer";
import BlogItem from "../BlogItem/BlogItem";
import Header from "../Header/Header";
import {
  setUser,
  setPage,
  setToken,
  setBlog,
  setSearch,
  User,
  CommentType,
  signinType,
  signupType,
} from "../../redux/actions/Payload";
import "./Blogs.scss";

// create a component named Blogs
const Blogs = () => {
  const location = useLocation();
  const history = useHistory();
  const actions = bindActionCreators(
    {
      setUser,
      setPage,
      setToken,
      setSearch,
      setBlog,
    },
    useDispatch()
  );

  const logger: signinType = useSelector((state: RootState) => state.logger);
  const app: appState = useSelector((state: RootState) => state.app);
  const register: signupType = useSelector(
    (state: RootState) => state.register
  );

  const { token, user, users, blogs, comments, page, search } = app;
  const { email: registerEmail } = register;
  const { email: loginEmail } = logger;

  const updateAppState = async () => {
    const usersArray = await getUserInfo(token);
    const comments = await getComments(token);
    const blogs = await getBlogs(token);

    const firstLogged = sessionStorage.getItem("firstLogged");
    const email = loginEmail || registerEmail;
    const userObject =
      typeof usersArray === typeof [] &&
      usersArray.map((item: User) => item.email === email && item);

    actions.setPage(location.pathname);
    actions.setUser({
      user: userObject[0],
      users: usersArray,
      blogs,
      comments,
    });

    if (firstLogged && firstLogged !== "false" && userObject[0].name !== "")
      message.success("Wellcome " + userObject[0].name);
  };

  /** ---------------------   METHODS   --------------------- */

  const handleWriteBlog = () => {
    actions.setBlog({
      id: blogs.length + 1,
      userId: user.id,
      title: "",
      description: "",
    });

    history.push("/write-blog");
  };

  const scrollTotop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  const getFirstTwoComments = (blogId: number) => {
    let firstTwoComments: string[] = [];
    comments.map((itemComment: CommentType) => {
      if (itemComment.blogsId === blogId) {
        if (firstTwoComments.length < 3)
          firstTwoComments.push(itemComment.comment);
      }
    });

    return firstTwoComments;
  };

  useEffect(() => {
    // On first render
    if (token !== "") {
      updateAppState();
    } else {
      history.push("/signin");
    }
    return () => {
      sessionStorage.setItem("firstLogged", "false");
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const BlogItems =
    typeof blogs === typeof [] &&
    blogs.map((item: Blog, index: number) => {
      const author = users[item.userId - 1]?.name || "Anonymus";
      const firstComments = getFirstTwoComments(item.id);

      return (
        <BlogItem
          key={index.toString()}
          title={item.title}
          author={author}
          blog={item.description}
          comments={firstComments}
          index={index}
        />
      );
    });

  const YourBLogItems =
    typeof blogs === typeof [] &&
    blogs.map((item: any, index: number) => {
      const author = users[item.userId - 1]?.name || "Anonymus";
      const firstComments = getFirstTwoComments(item.id);

      if (author === user.name) {
        return (
          <BlogItem
            key={index.toString()}
            title={item.title}
            author={author}
            blog={item.description}
            comments={firstComments}
            index={index}
          />
        );
      }
    });

  const SearchItems =
    typeof blogs === typeof [] &&
    blogs.map((item: any, index: number) => {
      const author = users[item.userId - 1]?.name || "Anonymus";
      const firstComments = getFirstTwoComments(item.id);
      const title = item.title;

      if (page === "/your-blogs") {
        if (
          title.toLowerCase().includes(search.toLowerCase()) &&
          author === user.name
        ) {
          return (
            <BlogItem
              key={index.toString()}
              title={title}
              author={author}
              blog={item.description}
              comments={firstComments}
              index={index}
            />
          );
        }
      } else {
        if (title.toLowerCase().includes(search.toLowerCase())) {
          return (
            <BlogItem
              key={index.toString()}
              title={title}
              author={author}
              blog={item.description}
              comments={firstComments}
              index={index}
            />
          );
        }
      }
    });

  return (
    <div className="blogs-container">
      <Header />
      <div className="blogs">
        {search !== ""
          ? SearchItems
          : page !== "/your-blogs"
          ? BlogItems
          : YourBLogItems}
        <Affix
          className="write-blog-affix"
          offsetTop={20}
          style={{ position: "fixed", top: "13vh", left: "49%", right: "50%" }}
        >
          <Button type="link" onClick={handleWriteBlog}>
            <PlusCircleOutlined style={{ fontSize: 24 }} />
          </Button>
        </Affix>
        <Affix
          offsetBottom={20}
          style={{ position: "fixed", bottom: 20, right: 20 }}
        >
          <Button type="primary" onClick={scrollTotop}>
            Go Up
          </Button>
        </Affix>
      </div>
    </div>
  );
};

//make this component available to the app
export default Blogs;
