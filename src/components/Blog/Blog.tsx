//import liraries
import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { Card, List, Comment, Input, Button } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { RootState } from "../../redux/reducers/CombineReducers";

import {
  setComment,
  setEdit,
  setUser,
  commentType,
  setBlog,
  CommentType,
} from "../../redux/actions/Payload";
import {
  deleteBlog,
  deleteComment,
  addComment,
  updateComment,
  getComments,
} from "../../API_Calls";
import { appState } from "../../redux/reducers/appReducer";
import Header from "../Header/Header";
import "./Blog.scss";

// create a component named Blog
const Blog = () => {
  const app: appState = useSelector((state: RootState) => state.app);
  const editComment: commentType = useSelector(
    (state: RootState) => state.editComment
  );

  const history = useHistory();
  const { id } = useParams();

  const { TextArea, Search } = Input;
  const { token, user, users, blogs, comments, edit } = app;

  const username = user.name;
  const blog = blogs[id];
  const author = (blog && users[blog.userId - 1]?.name) || "Anonymus";
  let commentData = [];

  /** -----------------   EXTRACT ACTIONS   ----------------- */

  const appActions = bindActionCreators({ setUser, setEdit }, useDispatch());
  const blogActions = bindActionCreators({ setBlog }, useDispatch());
  const commentActions = bindActionCreators({ setComment }, useDispatch());

  /** -----------------   GET RELEVANT COMMENTS IN AN []    ----------------- */
  const commentsIndex = comments.map((item: CommentType, index: number) => {
    return {
      id: item.blogsId === blog.id ? index.toString() : "",
    };
  });

  /** -----------------   BLOG METHODS    ----------------- */

  const handleEditBlog = () => {
    blogActions.setBlog({
      id: blog.id,
      userId: user.id,
      title: blog.title,
      description: blog.description,
    });

    history.push("/blogs/" + blog.id + "/edit");
  };

  const handleDeleteBlog = () => {
    deleteBlog(token, blog.id);
    history.push("/blogs");
  };

  /** -----------------   COMMENT METHODS   ----------------- */

  const refreshComments = async () => {
    const gotComments = await getComments(token);

    appActions.setUser({
      user,
      users,
      blogs,
      comments: gotComments,
    });
  };

  const handleAddComment = () => {
    if (editComment.comment !== "")
      addComment(token, editComment.blogsId, editComment);

    refreshComments();
  };

  const handleUpdateComment = () => {
    if (editComment.comment !== "") {
      updateComment(token, editComment.commentId, editComment);

      appActions.setEdit("");
      refreshComments();
    }
  };

  const handleEditMode = (index: number) => {
    if (edit === "") {
      appActions.setEdit(index.toString());
    } else {
      appActions.setEdit("");
    }
  };

  const handleEditComment = (e: any) => {
    const commentId =
      edit === ""
        ? comments.length + 1
        : e.currentTarget.getAttribute("data-key");

    commentActions.setComment({
      commentId,
      userId: user.id,
      blogsId: blog.id,
      comment: e.target.value,
    });
  };

  const handleDeleteComment = async (e: any) => {
    const commentId = e.currentTarget.getAttribute("data-key");
    deleteComment(token, commentId);

    refreshComments();
  };

  /** --------------    PUSH COMMENTS TO []   -------------- */
  for (let i in comments) {
    if (i === commentsIndex[i].id) {
      const item = comments[i];
      const author = users[item.userId - 1].name + ":";
      commentData.push({
        id: item.id,
        author,
        content: item.comment,
      });
    }
  }

  useEffect(() => {
    /** ------------------    USER VALIDATION   ------------------ */
    token === "" && history.push("/signin");
    return () => {};
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const title = (
    <div className="blog-title">
      <p>{blog && blog.title}</p>
      {blog && user.id === blog.userId && (
        <div className="blog-edit-buttons">
          <span onClick={handleEditBlog}>
            <EditOutlined />
          </span>
          <span onClick={handleDeleteBlog}>
            <DeleteOutlined />
          </span>
        </div>
      )}
    </div>
  );

  /** ---------------------   COMMENT ACTIONS   --------------------- */

  const getActions = (item: any, index: number) => {
    return [
      <span
        key={index.toString()}
        data-key={index}
        onClick={() => handleEditMode(index)}
      >
        <EditOutlined />
      </span>,
      <span
        key={index.toString()}
        data-key={index}
        onClick={handleDeleteComment}
      >
        <DeleteOutlined />
      </span>,
    ];
  };

  return (
    <div className="blog-container">
      <Header />
      <Card title={title} className="blog-item">
        <span>{blog && blog.description}</span>
        <div className="footer">
          <p className="author">
            Author: <span>{author}</span>
          </p>
        </div>
      </Card>
      {commentData.length !== 0 ? (
        <List
          className="comment-list"
          header={`${commentData.length} replies`}
          dataSource={commentData}
          renderItem={(item, index) => (
            <>
              <Comment
                data-key={item.id}
                className="comment"
                author={item.author}
                content={
                  edit === item.id.toString() ? (
                    <Search
                      data-key={item.id}
                      defaultValue={item.content.toString()}
                      enterButton="Comment"
                      onChange={handleEditComment}
                      onPressEnter={handleUpdateComment}
                      onSearch={handleUpdateComment}
                    />
                  ) : (
                    item.content
                  )
                }
                actions={
                  item.author.includes(username)
                    ? getActions(item, item.id)
                    : []
                }
              />
              {index === commentData.length - 1 && (
                <div className="write-comment">
                  <TextArea
                    placeholder="Write comment"
                    autoSize={{ minRows: 1, maxRows: 4 }}
                    onChange={handleEditComment}
                  />
                  <Button type="primary" onClick={handleAddComment}>
                    Comment
                  </Button>
                </div>
              )}
            </>
          )}
        />
      ) : (
        <div className="write-comment" style={{ width: "35vw" }}>
          <TextArea
            placeholder="Write comment"
            autoSize={{ minRows: 1, maxRows: 4 }}
            onChange={handleEditComment}
          />
          <Button type="ghost" onClick={handleAddComment}>
            Comment
          </Button>
        </div>
      )}
    </div>
  );
};

//make this component available to the app
export default Blog;
