//import liraries
import React from "react";
import { Card, Button, Popover } from "antd";
import { useHistory } from "react-router-dom";

import "./BlogItem.scss";

/** ----------------    DEFINE TYPES && INTERFACES    ---------------- */

type CommentT = {
  comment: string;
};

interface Blog {
  title: string;
  author: string;
  blog: string;
  comments: string[];
  index: number;
}

// create a component named BlogItem
const BlogItem = ({ title, author, blog, comments, index }: Blog) => {
  const history = useHistory();

  const hanldeRead = () => {
    history.push("/blogs/" + index);
  };

  const Comment = ({ comment }: CommentT) => {
    return (
      <div className="hover-comments">
        <span>{comment}</span>
      </div>
    );
  };

  const CommentsTitle = <span className="comments-title">Comments</span>;

  /** ---------------- GET THE FIRST 2 COMMENTS OF A BLOG   ---------------- */
  const Comments =
    comments.length > 0 ? (
      comments.map(
        (item: string, index: number) =>
          index < 2 && <Comment key={index.toString()} comment={item} />
      )
    ) : (
      <Comment key={"0"} comment="No comments yet!" />
    );

  return (
    <Popover placement="right" title={CommentsTitle} content={Comments}>
      <Card
        className="item"
        title={title}
        style={{ width: "100%", maxHeight: "37vh" }}
        extra={
          <Button type="primary" onClick={hanldeRead}>
            Read
          </Button>
        }
      >
        <span>{blog.slice(0, 118)}...</span>
        <div className="footer">
          <p className="author">
            Author: <span>{author}</span>
          </p>
        </div>
      </Card>
    </Popover>
  );
};

//make this component available to the app
export default BlogItem;
