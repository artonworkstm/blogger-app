//import liraries
import React from "react";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { Form, Input, Button, Checkbox, message } from "antd";

import { setLogin, setToken, signinType } from "../../redux/actions/Payload";
import { RootState } from "../../redux/reducers/CombineReducers";
import { loginUser } from "../../API_Calls";
import "./Signin.scss";

// create a component named Signin
const Signin = () => {
  const logger: signinType = useSelector((state: RootState) => state.logger);

  const { email, password, remember } = logger;

  const history = useHistory();

  const actions = bindActionCreators({ setLogin, setToken }, useDispatch());

  /** --------------------------    METHODS   -------------------------- */

  const onFinish = async () => {
    const token = await loginUser(email, password).then((data) => {
      actions.setToken(data);
      if (data) {
        remember
          ? localStorage.setItem("username", email)
          : localStorage.removeItem("username");
        sessionStorage.setItem("firstLogged", email);
        history.push("/");
      } else {
        message.error("Authentication error!");
      }
    });
  };

  const onFinishFailed = () => {
    if (email.length < 1 || password.length < 1) {
      message.error("Please fill the forms!");
    } else {
      message.error("Authentication error!");
    }
  };

  const handleEmail = (e: any) => {
    actions.setLogin({ email: e.target.value, password, remember });
  };

  const handlePassword = (e: any) => {
    actions.setLogin({ email, password: e.target.value, remember });
  };

  const handleRemember = (e: any) => {
    actions.setLogin({ email, password, remember: e.target.value });
  };

  const handleRedirect = () => {
    history.push("/signup");
  };

  return (
    <div className="sign">
      <h1 className="title">Blogg-in</h1>
      <Form
        className="form"
        name="signin-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          name="Email"
          rules={[{ required: true, message: "Please input your e-mail!" }]}
          initialValue={email}
        >
          <Input placeholder="Email" onChange={handleEmail} />
        </Form.Item>
        <Form.Item
          name="Password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password placeholder="Password" onChange={handlePassword} />
        </Form.Item>
        <Form.Item name="remember" valuePropName="checked">
          <Checkbox style={{ width: "100%" }} onChange={handleRemember}>
            Remember me
          </Checkbox>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" style={{ width: "100%" }}>
            Signin
          </Button>
        </Form.Item>
        <Form.Item>
          <Button
            type="link"
            htmlType="button"
            style={{
              width: "100%",
              padding: 0,
              display: "flex",
              justifyContent: "flex-start",
            }}
            onClick={handleRedirect}
          >
            Don't have an account ? Register here
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

//make this component available to the app
export default Signin;
