import { combineReducers } from "redux";
import loginReducer from "./loginReducer";
import appReducer from "./appReducer";
import registerReducer from "./registerReducer";
import blogReducer from "./blogReducer";
import commentReducer from "./commentReducer";

const rootReducer = combineReducers({
  logger: loginReducer,
  register: registerReducer,
  editBlog: blogReducer,
  editComment: commentReducer,
  app: appReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
