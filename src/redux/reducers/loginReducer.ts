import { SIGNIN } from "../actions/Types";
import { signinType, signinAction } from "../actions/Payload";

const email = String(localStorage.getItem("username") || "") || "";

const initialState: signinType = {
  email,
  password: "",
  remember: true,
};

export default (state = initialState, action: signinAction) => {
  switch (action.type) {
    case SIGNIN:
      return {
        ...state,
        email: action.payload?.email,
        password: action.payload?.password,
        remember: action.payload?.remember,
      };

    default:
      return state;
  }
};
