import { COMMENT } from "../actions/Types";
import { commentType, commentAction } from "../actions/Payload";

const initialState: commentType = {
  commentId: 0,
  userId: 0,
  blogsId: 0,
  comment: "",
};

export default (state = initialState, action: commentAction) => {
  switch (action.type) {
    case COMMENT:
      return {
        ...action.payload,
      };

    default:
      return state;
  }
};
