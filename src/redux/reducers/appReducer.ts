import { APP } from "../actions/Types";
import { appUserAction, User, Blog, CommentType } from "../actions/Payload";

export type appState = {
  token: string;
  user: User;
  users: User[];
  blogs: Blog[];
  comments: CommentType[];
  page: string;
  edit: string;
  search: string;
};

const initialState: appState = {
  token: "",
  user: { email: "", password: "", name: "", id: 0 },
  users: [],
  blogs: [],
  comments: [],
  page: "",
  edit: "",
  search: "",
};

export default (state = initialState, action: appUserAction) => {
  switch (action.type) {
    case APP.TOKEN:
      return {
        ...state,
        token: action.payload,
      };
    case APP.USER:
      return {
        ...state,
        user: action.payload?.user,
        users: action.payload?.users,
        blogs: action.payload?.blogs,
        comments: action.payload?.comments,
      };
    case APP.PAGE:
      return {
        ...state,
        page: action.payload,
      };
    case APP.EDIT:
      return {
        ...state,
        edit: action.payload,
      };
    case APP.SEARCH:
      return {
        ...state,
        search: action.payload,
      };

    default:
      return state;
  }
};
