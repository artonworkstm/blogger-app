import { SIGNUP } from "../actions/Types";
import { signupType, signupAction } from "../actions/Payload";

const initialState: signupType = {
  email: "",
  firstName: "",
  lastName: "",
  password: "",
  repassword: "",
};

export default (state = initialState, action: signupAction) => {
  switch (action.type) {
    case SIGNUP:
      return {
        ...state,
        email: action.payload?.email,
        firstName: action.payload?.firstName,
        lastName: action.payload?.lastName,
        password: action.payload?.password,
        repassword: action.payload?.repassword,
      };

    default:
      return state;
  }
};
