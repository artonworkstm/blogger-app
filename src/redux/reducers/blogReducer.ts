import { BLOG } from "../actions/Types";
import { blogType, blogAction } from "../actions/Payload";

const initialState: blogType = {
  id: 0,
  userId: 0,
  title: "",
  description: "",
};

export default (state = initialState, action: blogAction) => {
  switch (action.type) {
    case BLOG:
      return {
        ...state,
        id: action.payload?.id,
        userId: action.payload?.userId,
        title: action.payload?.title,
        description: action.payload?.description,
      };

    default:
      return state;
  }
};
