/**   ---------------------   STRING TYPES   --------------------- */

export const SIGNIN = "SIGNIN";

export const SIGNUP = "SIGNUP";

export const APP = {
  TOKEN: "TOKEN",
  USER: "USER",
  PAGE: "PAGE",
  EDIT: "EDIT",
  SEARCH: "SEARCH",
};

export const COMMENT = "COMMENT";

export const BLOG = "BLOG";
