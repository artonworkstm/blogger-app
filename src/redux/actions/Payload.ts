import { SIGNIN, SIGNUP, APP, BLOG, COMMENT } from "./Types";

/** -----------------------    LOG IN    ----------------------- */

export type signinType = {
  email: string;
  password: string;
  remember: boolean;
};

export interface signinAction {
  type: string;
  payload: signinType;
}

export const setLogin = (value: signinType): signinAction => ({
  type: SIGNIN,
  payload: value,
});

/** -----------------------    REGISTER    ----------------------- */

export type signupType = {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  repassword: string;
};

export interface signupAction {
  type: string;
  payload: signupType;
}

export const setRegister = (value: signupType): signupAction => ({
  type: SIGNUP,
  payload: value,
});

/** -----------------------    ATUHENTICATION TOKEN    ----------------------- */

export interface stringAction {
  type: string;
  payload: string;
}

export const setToken = (value: string): stringAction => ({
  type: APP.TOKEN,
  payload: value,
});

/** -----------------------    SET APP STATE    ----------------------- */

export type User = {
  id: number;
  name: string;
  email: string;
  password: string;
};

export type Blog = {
  id: number;
  userId: number;
  title: string;
  description: string;
};

export type CommentType = {
  id: number;
  blogsId: number;
  userId: number;
  comment: string;
};

export type appType = {
  user: User;
  users: User[];
  blogs: Blog[];
  comments: CommentType[];
};

export interface appUserAction {
  type: string;
  payload: appType;
}

export const setUser = (value: appType): appUserAction => ({
  type: APP.USER,
  payload: value,
});

export const setPage = (value: string): stringAction => ({
  type: APP.PAGE,
  payload: value,
});

export const setEdit = (value: string): stringAction => ({
  type: APP.EDIT,
  payload: value,
});

export const setSearch = (value: string): stringAction => ({
  type: APP.SEARCH,
  payload: value,
});

/** -----------------------    EDIT OR CREATE A BLOG    ----------------------- */

export type blogType = {
  id: number;
  userId: number;
  title: string;
  description: string;
};

export interface blogAction {
  type: string;
  payload: blogType;
}

export const setBlog = (value: blogType): blogAction => ({
  type: BLOG,
  payload: value,
});

/** -----------------------    EDIT OR CREATE A COMMENT    ----------------------- */

export type commentType = {
  commentId: number;
  userId: number;
  blogsId: number;
  comment: string;
};

export interface commentAction {
  type: string;
  payload: commentType;
}

export const setComment = (value: commentType): commentAction => ({
  type: COMMENT,
  payload: value,
});
