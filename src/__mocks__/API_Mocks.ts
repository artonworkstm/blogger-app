/** ------------------    Authentication    ------------------------ */

export const registerUserMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        accessToken:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5ld0BtYWlsLmNvbSIsImlhdCI6MTU5NjgxNDU4OCwiZXhwIjoxNTk2ODE4MTg4LCJzdWIiOiIyIn0.W6oG7uofhuNcUzBSTx-Tn4XXICFvp3p_VvIanHYnOJ0",
      }),
  })
);

export const loginUserMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        accessToken:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M",
      }),
  })
);

/** ------------------    User Info    ------------------------ */

export const getUserInfoMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        email: "test@mail.com",
        password:
          "$2a$10$uBQ9lhEiFEJHt8pYqfBK4el4H5/51QJc65wV8ukUqR/EpgLevciFC",
        id: 1,
      }),
  })
);

/** ------------------    Blogs    ------------------------ */

export const getBlogsMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve([
        {
          authorId: 1,
          title: "A wonderfull day",
          description: "Today was a wonderfull day...",
        },
        {
          authorId: 2,
          title: "Another journey",
          description: "I have decided to extend my...",
        },
        {
          authorId: 2,
          title: "Lame week",
          description: "Last week was a horrible disaster.",
        },
        {
          authorId: 1,
          title: "Still running",
          description: "The fear is still in me...",
        },
      ]),
  })
);

// with Id: 1
export const getYourBlogsMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve([
        {
          id: 1,
          userId: 1,
          title: "A wonderfull day",
          description: "Today was a wonderfull day...",
        },
        {
          id: 3,
          userId: 1,
          title: "Lame week",
          description: "Last week was a horrible disaster.",
        },
      ]),
  })
);

export const writeBlogMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        id: 6,
        authorId: 1,
        title: "A new phase",
        description: "In life we all..",
      }),
  })
);

export const updateBlogMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        id: 5,
        authorId: 2,
        title: "People are",
        description: "I have managed to...",
      }),
  })
);

export const deleteBlogMock = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({}),
  })
);

/** ------------------    Comments    ------------------------ */

// with Id: 1
export const getBlogCommentsMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve([
        {
          id: 1,
          blogsId: 1,
          userId: 2,
          comment: "This was beautiful, thanx for the story!",
        },
        {
          id: 2,
          blogsId: 1,
          userId: 1,
          comment: "Thank you for the kind words!",
        },
      ]),
  })
);

export const addCommentMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        id: 4,
        blogsId: 3,
        userId: 2,
        comment: "Next will be better!",
      }),
  })
);

export const updateCommentMock = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        id: 4,
        blogsId: 3,
        userId: 2,
        comment: "Hope next will be better!",
      }),
  })
);

export const deleteCommentMock = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({}),
  })
);
