//import liraries
import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Blogs from "../components/Blogs/Blogs";
import Signup from "../components/Signup/Signup";
import Signin from "../components/Signin/Signin";
import Blog from "../components/Blog/Blog";
import EditBlog from "../components/EditBlog/EditBlog";

// create a component named Router
const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/signin" component={Signin} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/" component={Blogs} />
        <Route exact path="/blogs" component={Blogs} />
        <Route exact path="/your-blogs" component={Blogs} />
        <Route exact path="/write-blog" component={EditBlog} />
        <Route exact path="/blogs/:id" component={Blog} />
        <Route exact path="/blogs/:id/edit" component={EditBlog} />
      </Switch>
    </BrowserRouter>
  );
};

//make this component available to the app
export default Router;
