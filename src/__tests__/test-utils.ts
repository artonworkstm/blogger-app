import React from "react";
import { mount } from "enzyme";
import rootReducer from "../redux/reducers/CombineReducers";
/**
 * makeMountRender
 *
 * makeStore
 *
 * reduxify
 *
 * snapshotify
 */

/** ---------------        makeMontRender      --------------- */

export const makeMountRender = (Component: JSX.Element, defaultProps = {}) => {
  return (customProps = {}) => {
    const props = {
      ...defaultProps,
      ...customProps,
    };
    return mount(Component, props);
  };
};

export const makeStore = (customState = {}) => {};
