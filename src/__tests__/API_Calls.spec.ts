import {
  loginUser,
  getUserInfo,
  registerUser,
  getBlogs,
  getYourBlogs,
  updateBlog,
  writeBlog,
  deleteBlog,
  addComment,
  updateComment,
  deleteComment,
} from "../API_Calls";
import {
  getUserInfoMock,
  loginUserMock,
  registerUserMock,
  getBlogsMock,
  getYourBlogsMock,
  updateBlogMock,
  writeBlogMock,
  deleteBlogMock,
  addCommentMock,
  updateCommentMock,
  deleteCommentMock,
} from "../__mocks__/API_Mocks";

describe("Authentication Calls", () => {
  it("Register user", async () => {
    fetch = registerUserMock;

    const token = await registerUser("new@gmail.com", "123456", "Test User");

    expect(token).toBe(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5ld0BtYWlsLmNvbSIsImlhdCI6MTU5NjgxNDU4OCwiZXhwIjoxNTk2ODE4MTg4LCJzdWIiOiIyIn0.W6oG7uofhuNcUzBSTx-Tn4XXICFvp3p_VvIanHYnOJ0"
    );
  });

  it("Logs user", async () => {
    fetch = loginUserMock;

    const token = await loginUser("test@gmail.com", "123456");

    expect(token).toBe(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M"
    );
  });
});

describe("User Info", () => {
  it("Get User Info", async () => {
    fetch = getUserInfoMock;

    const userInfo = await getUserInfo(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M"
    );

    expect(userInfo).toMatchObject({
      email: "test@mail.com",
      password: "$2a$10$uBQ9lhEiFEJHt8pYqfBK4el4H5/51QJc65wV8ukUqR/EpgLevciFC",
      id: 1,
    });
  });
});

describe("Blogs", () => {
  it("Get Blogs feed", async () => {
    fetch = getBlogsMock;

    const blogsFeed = await getBlogs(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M"
    );

    expect(blogsFeed).toMatchObject([
      {
        authorId: 1,
        title: "A wonderfull day",
        description: "Today was a wonderfull day...",
      },
      {
        authorId: 2,
        title: "Another journey",
        description: "I have decided to extend my...",
      },
      {
        authorId: 2,
        title: "Lame week",
        description: "Last week was a horrible disaster.",
      },
      {
        authorId: 1,
        title: "Still running",
        description: "The fear is still in me...",
      },
    ]);
  });

  it("Get Your Blogs feed", async () => {
    fetch = getYourBlogsMock;

    const yourBlogsFeed = await getYourBlogs(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M",
      1
    );

    expect(yourBlogsFeed).toMatchObject([
      {
        id: 1,
        userId: 1,
        title: "A wonderfull day",
        description: "Today was a wonderfull day...",
      },
      {
        id: 3,
        userId: 1,
        title: "Lame week",
        description: "Last week was a horrible disaster.",
      },
    ]);
  });

  it("Add Blog", async () => {
    fetch = writeBlogMock;

    const write = await writeBlog(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M",
      {
        id: 6,
        authorId: 1,
        title: "A new phase",
        description: "In life we all..",
      }
    );

    expect(write).toMatchObject({
      id: 6,
      authorId: 1,
      title: "A new phase",
      description: "In life we all..",
    });
  });

  it("Update Blog", async () => {
    fetch = updateBlogMock;

    const update = await updateBlog(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M",
      {
        id: 5,
        authorId: 2,
        title: "People are",
        description: "I have managed to...",
      }
    );
  });

  it("Delete Blog", async () => {
    fetch = deleteBlogMock;

    const deleteMethod = await deleteBlog(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M",
      6
    );

    expect(deleteMethod).toMatchObject({});
  });
});

describe("Comments", () => {
  it("Add Comment", async () => {
    fetch = addCommentMock;

    const add = await addComment(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M",
      1,
      {
        id: 4,
        blogsId: 3,
        userId: 2,
        comment: "Next will be better!",
      }
    );

    expect(add).toMatchObject({
      id: 4,
      blogsId: 3,
      userId: 2,
      comment: "Next will be better!",
    });
  });

  it("Update Comment", async () => {
    fetch = updateCommentMock;

    const update = await updateComment(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M",
      4,
      {
        id: 4,
        blogsId: 3,
        userId: 2,
        comment: "Hope next will be better!",
      }
    );

    expect(update).toMatchObject({
      id: 4,
      blogsId: 3,
      userId: 2,
      comment: "Hope next will be better!",
    });
  });

  it("Delete Comment", async () => {
    fetch = deleteCommentMock;

    const deleteMethod = deleteComment(
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbWFpbC5jb20iLCJpYXQiOjE1OTY4MDExNzYsImV4cCI6MTU5NjgwNDc3Niwic3ViIjoiMSJ9.wVTj839jo9gInJajD7FUOoLQkT8iN_KP7wI_SWaI27M",
      4
    );

    expect(deleteMethod).toMatchObject({});
  });
});
