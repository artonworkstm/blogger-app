import React from "react";
import { Provider } from "react-redux";

import "./App.scss";
import "antd/dist/antd.css";
import Router from "./Router/Router";
import store from "./redux/Store";

function App() {
  return (
    <Provider store={store}>
      <Router />
    </Provider>
  );
}

export default App;
