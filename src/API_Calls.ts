const url = "http://localhost:3000/";

export interface Blog {
  id: number;
  userId: number;
  title: string;
  description: string;
}

interface Comment {
  commentId: number;
  blogsId: number;
  userId: number;
  comment: string;
}

/** -----------------------   Authentication    ---------------------- */

export const loginUser = async (email: string, password: string) => {
  try {
    return await fetch(url + "login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((response) => response.json())
      .then((data) => data.accessToken);
  } catch (err) {
    console.log(err);
  }
};

export const registerUser = async (
  email: string,
  password: string,
  name: string,
  ...props: any
) => {
  try {
    return await fetch(url + "register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
        name,
        ...props,
      }),
    })
      .then((response) => response.json())
      .then((data) => data.accessToken);
  } catch (err) {
    console.log(err);
  }
};

/** -----------------------   User Info    ---------------------- */

export const getUserInfo = async (token: string) => {
  try {
    return await fetch(url + "users", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

/** -----------------------   Blogs    ---------------------- */

export const getBlogs = async (token: string) => {
  try {
    return await fetch(url + "blogs", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

export const getYourBlogs = async (token: string, userId: number) => {
  try {
    return await fetch(url + "blogs?userId=" + userId, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

export const writeBlog = async (token: string, blog: Blog) => {
  try {
    return await fetch(url + "blogs", {
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(blog),
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

export const updateBlog = async (token: string, blog: Blog) => {
  try {
    return await fetch(url + "blogs/" + blog.id, {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(blog),
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

export const deleteBlog = async (token: string, blogId: number) => {
  try {
    return await fetch(url + "blogs/" + blogId, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

/** -----------------------   Comments    ---------------------- */

export const getComments = async (token: string) => {
  try {
    return await fetch(url + "comments/", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

export const getBlogComments = async (token: string, blogId: number) => {
  try {
    return await fetch(
      url + "blogs/" + blogId + "/comments?blogsId=" + blogId,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    )
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

export const addComment = async (
  token: string,
  blogId: number,
  comment: Comment
) => {
  try {
    return await fetch(url + "comments", {
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(comment),
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

export const updateComment = async (
  token: string,
  commentId: number,
  comment: Comment
) => {
  try {
    return await fetch(url + "comments/" + commentId, {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(comment),
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};

export const deleteComment = async (token: string, commentId: number) => {
  try {
    return await fetch(url + "comments/" + commentId, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((data) => data);
  } catch (err) {
    console.log(err);
  }
};
